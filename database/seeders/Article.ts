import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

import { ArticleFactory } from "Database/factories";

export default class ArticleSeeder extends BaseSeeder {
  public async run() {
    // Write your database queries inside the run method
    await ArticleFactory.createMany(5)
  }
}

import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ArticleTag extends BaseSchema {
  protected tableName = 'article_tag'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('tag_id').unsigned().references('tags.id')
      table.integer('article_id').unsigned().references('articles.id')

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
